package de.f2k1.zollauktion;

public class Bieterliste {
    private String name;
    private String gebot;
    private String datum;

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    public String getGebot() {
        return gebot;
    }

    protected void setGebot(String gebot) {
        this.gebot = gebot;
    }

    public String getDatum() {
        return datum;
    }

    protected void setDatum(String datum) {
        this.datum = datum;
    }

    public Bieterliste() {

    }

}
