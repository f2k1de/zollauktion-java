package de.f2k1.zollauktion;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Auktion {
    private String title;
    private int id;
    private String anbieter;
    private String hoechstbietender;
    private String restzeit;
    private int anzahlgebote;
    private String anfangsgebot;
    private String gebotserhoehung;
    private String minimalgebot;
    private int viewcounter;
    private int bilderanzahl;
    private String description;
    private List<Bieterliste> bieterliste = new ArrayList<>();

    public List<Bieterliste> getBieterliste() {
        return bieterliste;
    }

    private void setBieterliste(List<Bieterliste> bieterliste) {
        this.bieterliste = bieterliste;
    }

    public Auktion(int id) {
        String url = "https://www.zoll-auktion.de/auktion/auktion.php?gebotsuebersicht=1&id=" + id;
        List<String> auktionsinfos = null;

        try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();

            Elements anbieterelem = doc.select("#auktionsinfos_liste_oben").first().children();

            setId(id);
            setAnbieter(anbieterelem.select("#auktionsinfos_anbieter").text().replace("Anbieter: ", ""));
            setTitle(doc.select("#ueberschrift_auktion").first().text());
            setViewcounter(Integer.parseInt(anbieterelem.select("#seitenaufruf").select(".content_bold").text()));
            setDescription(doc.select("#auktionsinfos_beschreibung").html());
            setBilderanzahl(Integer.parseInt(doc.select("#bildgross_anz").text()));
            auktionsinfos = anbieterelem.select(".auktionsinfos_rahmen").eachText();
            for(int i = 0; i < doc.select("#history").select("tbody").select("tr").size(); i++) {
                Bieterliste bieterliste1 = new Bieterliste();

                bieterliste1.setDatum(doc.select("#history").select("tbody").select("tr").get(i).select("td").get(2).text());
                bieterliste1.setGebot(doc.select("#history").select("tbody").select("tr").get(i).select("td").get(1).text());
                bieterliste1.setName(doc.select("#history").select("tbody").select("tr").get(i).select("td").get(0).text());
                bieterliste.add(bieterliste1);
            }
            //System.out.print(doc.select("#history").select("tbody").select("tr").get(1).select("td"));


        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String info : auktionsinfos
        ) {

            String firstword = info.split(" ", 2)[0];
            switch (firstword) {
                case "Höchstbieter:":
                    setHoechstbietender(info.replace("Höchstbieter: ", ""));
                    break;
                case "verbleibende":
                    setRestzeit(info.replace("verbleibende Zeit: ", ""));
                    break;
                case "Anzahl":
                    if (info.strip().equals("Anzahl der Gebote: 0")) {
                        setAnzahlgebote(Integer.parseInt(info.replaceAll("Anzahl der Gebote: ", "").strip()));
                    } else {
                        setAnzahlgebote(Integer.parseInt(info.substring(0, info.indexOf("(")).replaceAll("Anzahl der Gebote: ", "").strip()));
                    }
                    break;
                case "Anfangsgebot:":
                    setAnfangsgebot(info.replace("Anfangsgebot: ", ""));
                    break;
                case "Gebotserhöhung:":
                    setGebotserhoehung(info.replace("Gebotserhöhung: ", ""));
                    break;
                case "Minimalgebot:":
                    setMinimalgebot(info.replace("Minimalgebot: ", ""));
                    break;
            }
        }

    }

    public int getBilderanzahl() {
        return bilderanzahl;
    }

    private void setBilderanzahl(int bilderanzahl) {
        this.bilderanzahl = bilderanzahl;
    }

    public String[] getBilderUrl() {
        return getBilderUrl(false);
    }

    public String[] getBilderGallerieUrl() {
        return getBilderUrl(true);
    }

    public String[] getBilderUrl(boolean gallery) {
        String[] bilderlinks = new String[getBilderanzahl()];
        for (int i = 0; i < getBilderanzahl(); i++) {

            bilderlinks[i] = "https://www.zoll-auktion.de/auktion/daten/bilder_auktionen/";
            if (gallery) {
                bilderlinks[i] = bilderlinks[i] + "galerie_";
            }
            bilderlinks[i] = bilderlinks[i] + getId() + "_" + (i + 1) + ".jpg";
        }
        return bilderlinks;
    }

    public String getDescription() {
        return description;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    private void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    public String getAnbieter() {
        return anbieter;
    }

    private void setAnbieter(String anbieter) {
        this.anbieter = anbieter;
    }

    public String getHoechstbietender() {
        return hoechstbietender;
    }

    private void setHoechstbietender(String hoechstbietender) {
        this.hoechstbietender = hoechstbietender;
    }

    public String getRestzeit() {
        return restzeit;
    }

    private void setRestzeit(String restzeit) {
        this.restzeit = restzeit;
    }

    public int getAnzahlgebote() {
        return anzahlgebote;
    }

    private void setAnzahlgebote(int anzahlgebote) {
        this.anzahlgebote = anzahlgebote;
    }

    public String getAnfangsgebot() {
        return anfangsgebot;
    }

    private void setAnfangsgebot(String anfangsgebot) {
        this.anfangsgebot = anfangsgebot;
    }

    public String getGebotserhoehung() {
        return gebotserhoehung;
    }

    private void setGebotserhoehung(String gebotserhoehung) {
        this.gebotserhoehung = gebotserhoehung;
    }

    public String getMinimalgebot() {
        return minimalgebot;
    }

    private void setMinimalgebot(String minimalgebot) {
        this.minimalgebot = minimalgebot;
    }

    public int getViewcounter() {
        return viewcounter;
    }

    private void setViewcounter(int viewcounter) {
        this.viewcounter = viewcounter;
    }


}
