package de.f2k1.zollauktion;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BaldEndend {
    private List<Uebersichtsangebot> list = new ArrayList<>();

    public List<Uebersichtsangebot> getBaldEndend() {
        return list;
    }

    public BaldEndend() {
        String url = "https://www.zoll-auktion.de/auktion/";

        try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
            Element element = doc.select("#container").select("#Inhalt").select(".floatbox").select(".liste_startseite").last();
            Document html = Jsoup.parse(element.html());

            for(int i = 0; i < html.select("li").size(); i++) {
                Uebersichtsangebot uebersichtsangebot = new Uebersichtsangebot();
                uebersichtsangebot.setId(Integer.parseInt(html.select("li").get(i).select(".spalte2_startseite").select("a").attr("href").replace("auktion.php?id=", "")));
                uebersichtsangebot.setEndzeitpunkt(html.select("li").get(i).select(".spalte1_startseite").text());
                uebersichtsangebot.setTitle(html.select("li").get(i).select(".spalte2_startseite").text());
                list.add(uebersichtsangebot);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
