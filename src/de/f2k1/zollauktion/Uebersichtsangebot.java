package de.f2k1.zollauktion;

public class Uebersichtsangebot {
    int id;
    private String title, hinweisliste, standort, aktuellesgebot, anzahlgebote, restzeit, endzeitpunkt;

    public Uebersichtsangebot() {
    }

    public int getId() {
        return id;
    }

    protected void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHinweisliste() {
        return hinweisliste;
    }

    public void setHinweisliste(String hinweisliste) {
        this.hinweisliste = hinweisliste;
    }

    public String getStandort() {
        return standort;
    }

    public void setStandort(String standort) {
        this.standort = standort;
    }

    public String getAktuellesgebot() {
        return aktuellesgebot;
    }

    public void setAktuellesgebot(String aktuellesgebot) {
        this.aktuellesgebot = aktuellesgebot;
    }

    public String getAnzahlgebote() {
        return anzahlgebote;
    }

    public void setAnzahlgebote(String anzahlgebote) {
        this.anzahlgebote = anzahlgebote;
    }

    public String getRestzeit() {
        return restzeit;
    }

    public void setRestzeit(String restzeit) {
        this.restzeit = restzeit;
    }

    public String getEndzeitpunkt() {
        return endzeitpunkt;
    }

    public void setEndzeitpunkt(String endzeitpunkt) {
        this.endzeitpunkt = endzeitpunkt;
    }
}
