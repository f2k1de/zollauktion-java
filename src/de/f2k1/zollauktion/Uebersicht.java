package de.f2k1.zollauktion;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Uebersicht {

    private int aktuelleKat;
    private int letzteSeite;
    private int aktuelleSeite;
    private List<Uebersichtsangebot> angebote;

    public Uebersicht() {
        this(0, 0);
    }

    public Uebersicht(int katid, int seite) {
        setAktuelleSeite(seite);
        String url = "https://www.zoll-auktion.de/auktion/auktionsuebersicht.php";
        try {
            Document doc = Jsoup.connect(url).userAgent("Mozilla").get();
            Elements tablecontent = doc.select("#uebers_tab_auct_tbody").select(".bgw_tab");
            List<Uebersichtsangebot> uebersichtsangebote = new ArrayList<>();
            for (int i = 1; i < tablecontent.size(); i++) {
                Elements rowcontents = tablecontent.get(i).select("td");
                Uebersichtsangebot temp = new Uebersichtsangebot();

                temp.setHinweisliste(rowcontents.select(".hinweisliste").text());
                temp.setTitle(rowcontents.select("td").select("a").text());
                temp.setId(Integer.parseInt(rowcontents.select("td").select("a").attr("href").replace("auktion.php?id=", "")));
                temp.setAktuellesgebot(rowcontents.select(".gebotsangaben").select("li").get(0).text());
                temp.setAnzahlgebote(rowcontents.select(".gebotsangaben").select("li").get(1).text());
                temp.setRestzeit(rowcontents.select(".zeitangaben").select("li").get(0).text());
                temp.setEndzeitpunkt(rowcontents.select(".zeitangaben").select("li").get(1).text());
                temp.setStandort(rowcontents.select(".artikelstandort").text());

                uebersichtsangebote.add(temp);

            }
            setAngebote(uebersichtsangebote);
            setLetzteSeite(Integer.parseInt(doc.select("a").get(doc.select("a").size() - 3).attr("href").replace("auktionsuebersicht.php?seite=", "")));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int getAktuelleSeite() {
        return aktuelleSeite;
    }

    private void setAktuelleSeite(int aktuelleSeite) {
        this.aktuelleSeite = aktuelleSeite;
    }

    public List<Uebersichtsangebot> getAngebote() {
        return this.angebote;
    }

    private void setAngebote(List<Uebersichtsangebot> angebote) {
        this.angebote = angebote;
    }

    public int getAktuelleKat() {
        return aktuelleKat;
    }

    private void setAktuelleKat(int aktuelleKat) {
        this.aktuelleKat = aktuelleKat;
    }

    public int getLetzteSeite() {
        return letzteSeite;
    }

    private void setLetzteSeite(int letzteSeite) {
        this.letzteSeite = letzteSeite;
    }
}
